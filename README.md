# README 

# nRF24l01-radiolla etäluettava lämpötila ja kosteusmittaus #

Tiedoston tarkoituksena on kuvata harjoitustyön toteutusta. Tarkoituksena on rakentaa etä- ja keräyslaitteisto hyväksikäyttäen Arduino Pro Mini- kehitysalustoja (+5Vcc), jossa on lämpötila- ja kosteusmittaus tapahtuu HTD11-anturilla. Tiedonsiirto etälaitteelta kerääjälle (Raspberry Pi 3) tapahtuu hyväksikäyttämällä nRF24L01+-radiolähetinä. Etälaitteisiin tarvitaan patterille (9V) regulaattori muuntamaan jännite alustalle sopivaksi(+5Vcc), sekä muuttamaan alustan jännite radiolle sopivaksi (+3Vcc). Työssä tarkoituksena on hyväksikäyttää useampaa etäyksikköä (ks. kuvio alla).

![suunnitelma.png](https://bitbucket.org/repo/jgE8z6M/images/1488992977-suunnitelma.png) 



## Laitteistosuunnittelu dokumentit ##

### Lähettimen (Slave) tiedot: ###

Alustana etäluettavassa lämpötila- ja kosteusmittauksissa käytettään Pro Mini ATMEGA328P 5V 16MHz Development Module Boardia (ks. kuvio alla). Laitteiston virran lähteenä toimii 9V paristo. Jännite muutetaan regulaattoria hyväksikäyttäen laitteelle sopivaksi 5V jännitteeksi. Ulkopuolinen jännitelähde kytketään RAW pinniin.

Lämpötila- ja RH- kosteusmittaus toteutetaan DHT11 anturimoduulilla, jonka lähtö asetatkaan digitaalisen porttiin 5. 

Kehitysalustan syöttämä 5V jännite muutetaan regulaattorilla NRF24L01:lle sopivaksi 3.3 V:in jännitteeksi. NRF24L01 kytkentäkaavio on seuraavanlainen:

![ProMini_NRF24L01_kytkenta.png](https://bitbucket.org/repo/jgE8z6M/images/637124145-ProMini_NRF24L01_kytkenta.png)  
         


![lampotilajakosteusmittaus.png](https://bitbucket.org/repo/jgE8z6M/images/3341230488-lampotilajakosteusmittaus.png)

### Vastaanottimen (master) tiedot ###

Radion NRF24L01 kytkentä masterina toimivaan Raspberry Pi 3:een. Pinnin kytkentä on esitelty (ks. kuvio alla).

![RaspberryPi_NRF24L01_kytkenta.png](https://bitbucket.org/repo/jgE8z6M/images/1132833670-RaspberryPi_NRF24L01_kytkenta.png)
![Lampotilajakosteusmaster_bb.png](https://bitbucket.org/repo/jgE8z6M/images/1083414106-Lampotilajakosteusmaster_bb.png)


## Ohjelmistosuunnittelu dokumentit ##

Arduino Pro mini voidaan ohjelmoida käyttämällä USB-kaapelia (ks. [Arduino programming cable](https://www.amazon.co.uk/Sumind-Raspberry-Programming-Windows-Supported/dp/B01N4X3BJB/ref=sr_1_6?ie=UTF8&qid=1491943790&sr=8-6&keywords=Arduino++programming+cable)) tai käyttämällä toista Arduino alustaa (ks. [Pro Minin ohjelmointi](https://www.instructables.com/files/deriv/FD4/343G/GHI9CHD9/FD4343GGHI9CHD9.LARGE.jpg)).

### Lähettimessä (Slave) käytetty c-koodi: ###


```
#!c
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include <DHT11.h>
#include "Vcc.h"
#include "wtd.h"
#include <SPI.h>
#include <LowPower.h>

#define DHT_PIN 5// Set equal to channel sensor is on


/* ///////// DHT11 Kalibraatio ///////////////
 * Made in China DHT11
 * Kalibroinnnista (suhteellinen kosteus RH näyttää 10 % vähemmän, kuin todellinen kosteus) + noin 2-5%. EN OLE HUOMIOINUT TÄTÄ KOODISSA
 * Käytä mielummin tarkempaa DHT22-anturia.
 * Kosteusmittauksen kalibroinnista lisää ks.https://forum.mysensors.org/topic/126/calibrating-humidty/5
 */


///////////// Vcc //////////////////////////////////////////
// Jos käytetään patteria 9V - 6V sekä regulaattoria

const float VccMin   = 0.0;           // Minimum expected Vcc level, in Volts.
const float VccMax   = 5.0;           // Maximum expected Vcc level, in Volts.
const float VccCorrection = 1.0/1.0;  // Measured Vcc by multimeter divided by reported Vcc
Vcc vcc(VccCorrection);
// --------------------------------------------------------


#define channel        0x4c                  // nrf24 communication channel
#define writingPipe1   0xF0F0F0F0E1LL        // nrf24 communication address
#define writingPipe2   0xF0F0F0F0D2LL
#define writingPipe3   0xF0F0F0F0C3LL
#define writingPipe4   0xF0F0F0F0A4LL
#define writingPipe5   0xF0F0F0F0B5LL
#define dataRate       RF24_250KBPS          // nrf24 data rate (lower == more distance)
#define paLevel        RF24_PA_HIGH          // nrf24 power level

RF24 radio(9,10);

DHT11 dht11(DHT_PIN);

float payload[5];

////////CONFIGURE THESE FOR EACH NODE///////////////////////////////
char role = 'T'; // T for Transmitter(slave), R for Receiver(master)
char slaveID = '5';    //Slave node ID (1 - 5)

void setup(void) {
  Serial.begin(57600);
  printf_begin();
  pinMode(7,OUTPUT);

  radio.begin();
  radio.setRetries(15,15);
  radio.setPALevel(paLevel);
  radio.setChannel(channel);
  radio.enableDynamicPayloads();
  radio.setDataRate(dataRate);
  radio.setAutoAck(false);
  
  
  /////////Open pipes 1 - 5 for reading (master receives from slaves)
  radio.openReadingPipe(1, writingPipe1);
  radio.openReadingPipe(2, writingPipe2);
  radio.openReadingPipe(3, writingPipe3);
  radio.openReadingPipe(4, writingPipe4);
  radio.openReadingPipe(5, writingPipe5);

  /////////Open pipes 0 - 4 for writing (slaves send to master)
  if(slaveID=='1') radio.openWritingPipe(writingPipe1);
  if(slaveID=='2') radio.openWritingPipe(writingPipe2);
  if(slaveID=='3') radio.openWritingPipe(writingPipe3);
  if(slaveID=='4') radio.openWritingPipe(writingPipe4);
  if(slaveID=='5') radio.openWritingPipe(writingPipe5);
  radio.startListening();

  radio.printDetails();
  
  delay(1000);
}

void loop(void) {
 
/////////////TRANSMITTER////////////

  if(role=='T'){
    radio.stopListening();   //Slave stops listening and begins to transmit to master

     /*//////////////////Jännitteen säästö //////////////////////////
     * Jännitteen säästö kirjasto LowPower.h saatavilla https://github.com/rocketscream/Low-Power
     * Esimerkeissä on eri jännitteen säätöihin perustuen, kuten:
     * - > idleWakePeriodic 
     * - > powerDownWakeExternalInterrupt    
     * - > powerDownWakePeriodic 
     * - > standbyExternalInterruptSAMD21
     * Lisää aiheesta url: http://www.home-automation-community.com/arduino-low-power-how-to-run-atmega328p-for-a-year-on-coin-cell-battery/
      */

    // Enter power down state for 8 s with ADC and BOD module disabled
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); 

  
    char send_ = slaveID + '\0';

     int num = atoi(&send_);
     float id = (float)num;
     float temp, humi;
     dht11.read(humi, temp);
     
     /* //////////// Pro Mini jännitteen tilan seuraaminen //////////////////////////
      * Vcc.h saatavilla osoitteesta https://github.com/Yveaux/Arduino_Vcc
      * Example kansiossa tämän yksinkertaisen jännitteseurannan lisäksi
      * Vcc.Sleep, jänniteen säästö esimerkki
      */

     float voltage = vcc.Read_Volts(); // todellinen jännite
     float pvoltage = vcc.Read_Perc(VccMin, VccMax); // %-tila
     
     payload[0] = id;
     payload[1] = temp;
     payload[2] = humi;
     payload[3] = voltage;
     payload[4] = pvoltage;
        
     bool ok =  radio.write(payload, sizeof(payload));  //Send message

    // TEST
    if (ok)
      printf("ok...\n\r");  //OK if sending was successful
    else
      printf("failed.\n\r");
    
    delay(50);
    radio.startListening();  //Slave listens acknowledgement or instructions from master  
  }
  
   delay(1000);  //wait 1000 ms before next trial  
}

```
### Vastaanottimessa (Master) käytetty C++-koodi: ###

Raspberry Pi 3 tarvitsee NRF24L01+ radion käyttämiseksi tarvitaan RF24lle kirjasto, joka on ladattavissa [RF24](https://github.com/fergul/RF24). Kirjastojen käyttö on esitetty sen dokumentaatiossa.

```
// ohjelma.cpp
#!C++

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include "RF24.h"
#include <stdio.h>
#include <unistd.h>
#include "printf.h"

using namespace std;


#define channel     0x4c
#define writingPipe1    0x0F0F0F0F0E1LL
#define writingPipe2    0x0F0F0F0F0D2LL
#define writingPipe3    0x0F0F0F0F0C3LL
#define writingPipe4    0x0F0F0F0F0A4LL
#define writingPipe5    0x0F0F0F0F0B5LL
#define dataRate    RF24_250KBPS
#define paLevel     RF24_PA_HIGH    

RF24 radio(RPI_GPIO_P1_22,0);

float payload[5];
unsigned long  long nano = 1000000000;

char role = 'R'; // T for Transmitter(save), R for Receiver(master)
char slaveID = '5'; // Slave node ID (between 1 to 5)

void setup(void) {
    printf_begin();
    radio.begin();
    radio.setRetries(15,15);
    radio.setDataRate(dataRate);
    radio.setPALevel(paLevel);
    radio.setChannel(channel);
    radio.enableDynamicPayloads();
    radio.setAutoAck(false);

    /// Open pipes 1-5 fo reading (master receives from slaves)
    radio.openReadingPipe(1, writingPipe1);
    radio.openReadingPipe(2, writingPipe2);
    radio.openReadingPipe(3, writingPipe3);
    radio.openReadingPipe(4, writingPipe4);
    radio.openReadingPipe(5, writingPipe5);

    ///// Open pipes 0 -4 for writing (save send to master)
    if(slaveID=='1') radio.openWritingPipe(writingPipe1);
    if(slaveID=='2') radio.openWritingPipe(writingPipe2);
    if(slaveID=='3') radio.openWritingPipe(writingPipe3);
    if(slaveID=='4') radio.openWritingPipe(writingPipe4);
    if(slaveID=='5') radio.openWritingPipe(writingPipe5);

    radio.startListening();
    radio.printDetails();
    usleep(1000000);
}

void loop(void) {
    if(role=='R') {

        if(radio.available()) {
            bool done = false;
            while (!done) {
                radio.read(&payload, sizeof(payload));
                float q = payload[0];
                float w = payload[1];
                float e = payload[2];
                float r = payload[3];
                float t = payload[4];
                // TODO toimii, mutta voisi olla parempikin ratkaisu lukee (1-5 slaveID)
                                if((q > 0) && (q < 6)) {
                cout << q << " : " << w << " : " << e <<" : " << r << " : " << t << "\n" << endl;
                
            }else{
                        usleep(10000000);
            cout << "Huomio radio ei ole saatavilla\n" << endl;
            }
        usleep(1000000);    
	}
	}
    }
}

int main(int argc, char** argv) {
	try {
    	setup();
    		while(1){
        	loop();
    		usleep(10000000);
    		}
     }
catch (const exception &e) {
       cout << e.what() << endl;
       return EXIT_FAILURE;
       }
return EXIT_SUCCESS;
}


```
Tehdyn c++-ohjelman kääntäminen tapahtuu seuraavasti:

g++ -Wall -c ohjelma.cpp

g++ -Wall -o ohjelma ohjelma.o RF24.o spi.o bmc2835.o

sudo ./ohjelma



## Tiedon tallennus MongoDB ##

//// TODO ///////////////////////